﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.ObjectModel;

namespace TestApp_9Now
{
    public static class WebDriverExtensions
    {
        /// <summary>
        /// Gets the element found in the driver for the criteria, waiting until the specified time
        /// </summary>
        /// <param name="driver">The wedriver in which the element should be looked in</param>
        /// <param name="by">by what the element should be searched for</param>
        /// <param name="timeoutInSeconds">time in seconds to wait for the element to show up</param>
        /// <returns>the element found</returns>
        public static IWebElement FindElement(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            IWebElement element = null;
            try
            {
                if (timeoutInSeconds > 0)
                {
                    var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                    wait.IgnoreExceptionTypes(typeof(NoSuchElementException), typeof(ElementNotVisibleException), typeof(WebDriverTimeoutException));

                    element = wait.Until(SeleniumExtras.WaitHelpers.ExpectedConditions.ElementIsVisible(by));
                }
            }
            catch (WebDriverTimeoutException)
            {
                element = null;
            }
            return element;
        }
        /// <summary>
        /// Gets the collection of elements found in the driver for the criteria, waiting until the specified time
        /// </summary>
        /// <param name="driver">The wedriver in which the element should be looked in</param>
        /// <param name="by">by what the element should be searched for</param>
        /// <param name="timeoutInSeconds">time in seconds to wait for the element to show up</param>
        /// <returns>the collection of elements found</returns>
        public static ReadOnlyCollection<IWebElement> FindElements(this IWebDriver driver, By by, int timeoutInSeconds)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(timeoutInSeconds));
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
                wait.IgnoreExceptionTypes(typeof(WebDriverTimeoutException));

                return wait.Until(drv => (drv.FindElements(by).Count > 0) ? drv.FindElements(by) : null);
            }
            return driver.FindElements(by);
        }

        /// <summary>
        /// Gets the collection of elements found in the driver for the criteria
        /// </summary>
        /// <param name="driver">The wedriver in which the element should be looked in</param>
        /// <param name="Tag">This specifies the Html Tag of the element</param>
        /// <param name="Attri">Name or Key part of the attribure in the Tag</param>
        /// <param name="AttriVal">Value part of the attribure</param>
        /// <returns>the element found</returns>
        public static IWebElement FindElementByTagAndAttribute(this IWebDriver driver, string Tag, string Attri, string AttriVal)
        {
            IWebElement element;
            try
            {
                element = driver.FindElement(By.CssSelector(string.Format("{0}[{1}='{2}']", Tag, Attri, AttriVal)));
            }
            catch (NoSuchElementException)
            {
                element = null;
            }
            return element;
        }

    }
}
