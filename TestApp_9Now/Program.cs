﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApp_9Now.FunctionTests;

namespace TestApp_9Now
{
    class Program
    {
        static void Main(string[] args)
        {

            Stopwatch timer = new Stopwatch();
            //Timer Started
            timer.Start();



            var chromeDriver = Login.LoggingIn();
            var profileDetails = Profile.GetInfo(chromeDriver);
            Console.WriteLine(JsonConvert.SerializeObject(profileDetails, Formatting.Indented));
            Video.PremiumDrama(chromeDriver);
            Logout.LoggingOut(chromeDriver);





            //Timer Stopped
            timer.Stop();
            Console.WriteLine(" -Status: Complete, TimeTaken in minutes:" + timer.Elapsed.Minutes);

            Console.ReadLine();

        }
    }
}
