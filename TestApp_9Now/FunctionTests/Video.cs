﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp_9Now.FunctionTests
{
    public static class Video
    {
        public static void PremiumDrama(ChromeDriver driver)
        {
            try
            {
                //Navigate to login page
                driver.Navigate().GoToUrl("https://www.9now.com.au/");

                //Check if User successfully logged in
                var loginPerson = driver.FindElement(By.CssSelector("span[itemtype='https://schema.org/Person']"), 10);
                if (loginPerson != null)
                {
                    var premiumDramaList = driver.FindElements(By.XPath("//*[@id='content']/div/div[2]/div/div[2]/main/div/div[3]/section/div/div/div/ul/li"), 5);
                    var DramaUrlList = premiumDramaList.Select(d => d.FindElement(By.TagName("a")).GetAttribute("href")).ToList();

                    Console.WriteLine(" -Video:  {0} Premium Dramas found", DramaUrlList.Count());

                    Console.WriteLine(" -Video:  running only 3 dramas", DramaUrlList.Count());
                    foreach (var DramaUrl in DramaUrlList.Take(3))
                    {
                        driver.Navigate().GoToUrl(DramaUrl);
                        Console.WriteLine(" -Video: Navigated to the Premium Drama url: {0}", DramaUrl);

                        var episodeList = driver.FindElements(By.XPath("//*[@id='content']/div/div[2]/div/div[2]/main/div/div/div/div[2]/div[1]/section/div/div/div/ul/li"), 5);
                        var episodeUrlList = episodeList.Select(e => e.FindElement(By.TagName("a")).GetAttribute("href")).ToList();

                        Console.WriteLine(" -Video: {0} Episodes found for the drama", episodeUrlList.Count());
                        Console.WriteLine(" -Video:  running only 3 episodes");
                        foreach (var episodeUrl in episodeUrlList.Take(3))
                        {
                            driver.Navigate().GoToUrl(episodeUrl);

                            Console.WriteLine(" -Video: Navigated to the Episode url: {0}", episodeUrl);

                            var resume = driver.FindElement(By.XPath("//*[contains(@class,'_1VNN1W _30dEKg')]"), 3);
                            if (resume != null)
                            {
                                resume.Click();
                                Console.WriteLine(" -Video: clicked on resume button");
                            }
                        }

                    }
                }
                else
                {
                    Console.WriteLine(" -Sign in status:- " + "not logged in");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }

        }
    }
}
