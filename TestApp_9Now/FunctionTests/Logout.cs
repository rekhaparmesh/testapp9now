﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace TestApp_9Now.FunctionTests
{
    public static class Logout
    {
        public static void LoggingOut(ChromeDriver driver)
        {
            //Navigate to login page
            driver.Navigate().GoToUrl("https://www.9now.com.au/");

            //Check if User successfully logged in
            var loginPerson = driver.FindElement(By.CssSelector("span[itemtype='https://schema.org/Person']"), 10);
            if (loginPerson != null)
            {
                loginPerson.Click();
                var loginElement = driver.FindElement(By.XPath("//*[contains(text(), 'Log in')]"), 10);

                if (loginElement == null)
                {
                    Console.WriteLine(" -Sign in status:- " + "Successfully Logged Out");
                }
                else
                {
                    Console.WriteLine(" -Sign in status:- " + "Unable to Log Out");
                }

                //Calling close and then quit will kill the driver running process.
                driver.Close();
                driver.Quit();
            }
        }

    }
}
