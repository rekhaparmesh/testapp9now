﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestApp_9Now.FunctionTests
{

    public static class Profile
    {
        public static ProfileDTO GetInfo(ChromeDriver driver)
        {
            var profileDTO = new ProfileDTO();
            try
            {
                //Check if User successfully logged in
                var loginPerson = driver.FindElement(By.CssSelector("span[itemtype='https://schema.org/Person']"), 10);
                if (loginPerson != null)
                {
                    loginPerson.Click();
                    var myProfile = driver.FindElementByXPath("//*[contains(text(), 'My Profile')]");
                    myProfile.Click();
                    Console.WriteLine(" -Profile: Navigated to the Profile Page");
                    profileDTO.FirstName = driver.FindElement(By.Name("firstName")).GetAttribute("Value");
                    profileDTO.LastName = driver.FindElement(By.Name("lastName")).GetAttribute("Value");
                    profileDTO.Email = driver.FindElement(By.Name("email")).GetAttribute("Value");

                    IWebElement dobMonth = driver.FindElement(By.Name("dobMonth"));
                    SelectElement selectedValue = new SelectElement(dobMonth);
                    profileDTO.BirthMonth = selectedValue.SelectedOption.Text;

                    profileDTO.BirthYear = driver.FindElement(By.Name("dobYear")).GetAttribute("Value");
                    profileDTO.PostCode = driver.FindElement(By.Name("postCode")).GetAttribute("Value");

                    profileDTO.Gender = driver.FindElements(By.Name("gender")).Where(g => g.Selected).First().GetAttribute("value");

                }
                else
                {
                    Console.WriteLine(" -Sign in status:- " + "not logged in");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }


            return profileDTO;
        }
    }


    public class ProfileDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string BirthMonth { get; set; }
        public string BirthYear { get; set; }
        public string PostCode { get; set; }
        public string Gender { get; set; }
    }

}
