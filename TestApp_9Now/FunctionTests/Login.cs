﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;

namespace TestApp_9Now.FunctionTests
{
    public static class Login
    {
        public static ChromeDriver LoggingIn()
        {
            Console.WriteLine(" -Loggin in to 9Now site");
            ChromeDriverService srvics = ChromeDriverService.CreateDefaultService(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location));

            srvics.HideCommandPromptWindow = true;//Hides the services command prompt

            var options = new ChromeOptions();
            options.AddArgument("--proxy-server='direct://'");
            options.AddArgument("--proxy-bypass-list=*");
            options.AddArgument("--start-maximized");
            options.AddArgument("--no-sandbox");

            ChromeDriver driver = new ChromeDriver(srvics, options, TimeSpan.FromMinutes(4));
            try
            {
                var loginUrl = "https://login.nine.com.au/login?client_id=9nowweb&redirect_uri=%2F&category=9now_web&site=9now_Home&section=default&cta=9n_home_sitenav_sso_login";
                var emailId = "tt9319391@gmail.com";
                var password = "mewelcome1*";

                //Navigate to login page
                driver.Navigate().GoToUrl(loginUrl);

                //Enter Email id
                driver.FindElement(By.Name("email")).SendKeys(emailId);

                //Click on the Continue button
                driver.FindElementByTagAndAttribute("button", "data-testid", "submit-button_intelligent-login").Click();

                //Check if the email id is valid
                var error_email = driver.FindElementByTagAndAttribute("span", "data-testid", "input-error_email");
                if (error_email != null)
                {
                    Console.WriteLine(" -Sign in status:- " + error_email.Text);
                }
                else
                {
                    Console.WriteLine(" -Sign in status:- " + "Valid Email address");

                    //Enter password
                    driver.FindElement(By.Name("password"), 10).SendKeys(password);

                    Console.WriteLine(" -Sign in status:- " + "Loggin in User");

                    //Click on the Sign In button
                    driver.FindElementByTagAndAttribute("button", "data-testid", "submit-button_login").Click();

                    //Check if User successfully logged in
                    var loginPerson = driver.FindElement(By.CssSelector("span[itemtype='https://schema.org/Person']"), 10);

                    if (loginPerson != null)
                    {
                        Console.WriteLine(" -Sign in status:- " + "Successfully Logged in User: " + loginPerson.Text);
                    }
                    else
                    {
                        Console.WriteLine(" -Sign in status:- " + "Loggin in failed");
                    }

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            return driver;
        }

    }
}
